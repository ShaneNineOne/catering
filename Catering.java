import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import java.io.FileWriter;

public class Catering extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {   
        GridPane mainPane = new GridPane();        
        // Position mainPane for center       
        mainPane.setAlignment(Pos.CENTER);       
        // Set spaces between controls            
        mainPane.setHgap(20);   // Horizontal spacing to 20    
        mainPane.setVgap(10);   // Horizontal spacing to 10

        TextField tfname = new TextField("");
        TextField tfcontact = new TextField("");
        TextField tfguests = new TextField("");
        TextField tfcost = new TextField("$0");
        tfcost.setEditable(false);
        
        Label lbname = new Label("Customer's name");
        Label lbcontact = new Label("Customer's contact info");
   	 	Label lbguests = new Label("Number of guests");
   	 	
   	 	lbname.setPrefWidth(170);
   	 	lbcontact.setPrefWidth(170);
   	 	lbguests.setPrefWidth(170);
   	 	
	   	tfname.setOnMouseClicked(e -> 
	   	{
	   		tfname.selectAll();
	   	});
	   	tfcontact.setOnMouseClicked(e -> 
	   	{
	   		tfcontact.selectAll();
	   	});
	   	tfguests.setOnMouseClicked(e -> 
	   	{
	   		tfguests.selectAll();
	   	});
	   	
	   	tfguests.textProperty().addListener( e -> {
	   		int amount = parseIntIfPossible(tfguests.getText());
	   		if (amount == 0) {
	   			tfcost.setText("$0");
	   		} else {
	   			tfcost.setText("$" + amount * 35);
	   		}
	   	});
	   	
	   	tfname.setPrefWidth(245);
	   	tfcontact.setPrefWidth(245);
	   	tfguests.setPrefWidth(245);
	   	
	   	mainPane.add(lbname, 0, 0);
	   	mainPane.add(lbcontact, 0, 1);
	   	mainPane.add(lbguests, 0, 2);
	   	mainPane.add(tfname, 1, 0);
	   	mainPane.add(tfcontact, 1, 1);
	   	mainPane.add(tfguests, 1, 2);
	   	
	   	
        Button btChooseEntree = new Button("Choose Entree");
        btChooseEntree.setTextFill(Color.PURPLE);

        Font ckFont = Font.font("Arial", FontWeight.BOLD, 16);
        
        TextField tfChoiceEntree = new TextField("Chosen entree: ");
	    tfChoiceEntree.setPrefWidth(500);
	    tfChoiceEntree.setFont(ckFont);
	    tfChoiceEntree.setEditable(false);

	    RadioButton rbSalmon = new RadioButton("Salmon");
 	    rbSalmon.setTextFill(Color.PINK);
	    rbSalmon.setFont(ckFont);
	    RadioButton rbSteak = new RadioButton("Steak");
	    rbSteak.setTextFill(Color.BROWN);
	    rbSteak.setFont(ckFont);
	    RadioButton rbSalad = new RadioButton("Salad");
	    rbSalad.setTextFill(Color.GREEN);
	    rbSalad.setFont(ckFont);
	    RadioButton rbChicken= new RadioButton("Chicken");
	    rbChicken.setTextFill(Color.ORANGE);
	    rbChicken.setFont(ckFont); 
	    
        ToggleGroup entreeGroup = new ToggleGroup();
        
        rbSalmon.setToggleGroup(entreeGroup);
        rbSteak.setToggleGroup(entreeGroup);
        rbSalad.setToggleGroup(entreeGroup);
        rbChicken.setToggleGroup(entreeGroup);
	    
        mainPane.add(btChooseEntree, 1, 3);
        mainPane.add(rbSalmon, 2 , 3);        
        mainPane.add(rbSteak, 3 , 3);        
        mainPane.add(rbSalad, 4 , 3);        
        mainPane.add(rbChicken, 5, 3);              
        mainPane.add(tfChoiceEntree, 6, 3);

        btChooseEntree.setOnAction(e -> {    
            // Java code to execute on event fired to test check boxes 
            String ckResult = "Chosen entree: ";
            if(rbSalmon.isSelected())
                 ckResult = ckResult + "Salmon ";
            else if(rbSteak.isSelected())
                 ckResult = ckResult + "Steak ";
            else if(rbSalad.isSelected())
                 ckResult = ckResult + "Salad ";
            else if(rbChicken.isSelected())
                 ckResult = ckResult + "Chicken ";     
            tfChoiceEntree.setText(ckResult);
         });
        
        Button btChooseSide = new Button("Chosen sides: ");
        btChooseSide.setTextFill(Color.PURPLE);
        
        TextField tfChoiceSide = new TextField("Chosen sides: ");
	    tfChoiceSide.setPrefWidth(500);
	    tfChoiceSide.setFont(ckFont);
	    tfChoiceSide.setEditable(false);

	    CheckBox ckRice = new CheckBox("Rice");
 	    ckRice.setTextFill(Color.BURLYWOOD);
	    ckRice.setFont(ckFont);
	    CheckBox ckBroccoli = new CheckBox("Broccoli");
	    ckBroccoli.setTextFill(Color.GREEN);
	    ckBroccoli.setFont(ckFont);
	    CheckBox ckSoup = new CheckBox("Soup");
	    ckSoup.setTextFill(Color.DARKSALMON);
	    ckSoup.setFont(ckFont);
	    CheckBox ckPotato = new CheckBox("Potatoes");
	    ckPotato.setTextFill(Color.BROWN);
	    ckPotato.setFont(ckFont); 
        
        btChooseSide.setOnAction(e -> {    
            // Java code to execute on event fired to test check boxes 
            String ckResult = "Chosen sides: ";
            byte selectedCount = 0;
            if(ckRice.isSelected()) {
            	ckResult = ckResult + "Rice ";
            	selectedCount++;            	
            }
            if(ckBroccoli.isSelected()) {
            	ckResult = ckResult + "Broccoli ";
            	selectedCount++;            	
            }
            if(ckSoup.isSelected()) {            	
            	ckResult = ckResult + "Soup ";
            	selectedCount++;
            }
            if(ckPotato.isSelected()) {
            	ckResult = ckResult + "Potatoes ";   
            	selectedCount++;            	
            }
            	
            if (selectedCount > 2) {
            	ckRice.setSelected(false);
            	ckBroccoli.setSelected(false);
            	ckSoup.setSelected(false);
            	ckPotato.setSelected(false);
            	ckResult = "Chosen sides: ";
            }
            tfChoiceSide.setText(ckResult);
         });
        
        mainPane.add(btChooseSide, 1, 4);
        mainPane.add(ckRice, 2 , 4);        
        mainPane.add(ckBroccoli, 3 , 4);        
        mainPane.add(ckSoup, 4 , 4);        
        mainPane.add(ckPotato, 5, 4);              
        mainPane.add(tfChoiceSide, 6, 4);
        
        Button btChooseDessert = new Button("Choose Dessert");
        btChooseDessert.setTextFill(Color.PURPLE);

        TextField tfChoiceDessert = new TextField("Chosen dessert: ");
	    tfChoiceDessert.setPrefWidth(500);
	    tfChoiceDessert.setFont(ckFont);
	    tfChoiceDessert.setEditable(false);

	    RadioButton rbCake = new RadioButton("Cake");
 	    rbCake.setTextFill(Color.BLUEVIOLET);
	    rbCake.setFont(ckFont);
	    RadioButton rbPudding = new RadioButton("Pudding");
	    rbPudding.setTextFill(Color.GOLDENROD);
	    rbPudding.setFont(ckFont);
	    RadioButton rbParfait = new RadioButton("Parfait");
	    rbParfait.setTextFill(Color.GREEN);
	    rbParfait.setFont(ckFont);
	    
	    ToggleGroup dessertGroup = new ToggleGroup();
        
        rbCake.setToggleGroup(dessertGroup);
        rbPudding.setToggleGroup(dessertGroup);
        rbParfait.setToggleGroup(dessertGroup);
        
        mainPane.add(btChooseDessert, 1, 5);
        mainPane.add(rbCake, 2 , 5);        
        mainPane.add(rbPudding, 3 , 5);        
        mainPane.add(rbParfait, 4 , 5);                
        mainPane.add(tfChoiceDessert, 6, 5);
        
        btChooseDessert.setOnAction(e -> {    
            // Java code to execute on event fired to test check boxes 
            String ckResult = "Chosen dessert: ";
            if(rbCake.isSelected())
                 ckResult = ckResult + "Cake ";
            else if(rbPudding.isSelected())
                 ckResult = ckResult + "Pudding ";
            else if(rbParfait.isSelected())
                 ckResult = ckResult + "Parfait ";
            tfChoiceDessert.setText(ckResult);
         });
        
        
        Label lbcost = new Label("Total cost ");
        
        mainPane.add(lbcost, 0, 6);
        mainPane.add(tfcost, 1, 6);
       
        Button btValidate = new Button("Validate Selection");
        Label lbresult = new Label("");
        Font validationFont = Font.font("Arial", FontWeight.BOLD, 20);
        lbresult.setFont(validationFont);
        
        btValidate.setOnAction(e -> {
        	int cost = parseCost(tfcost.getText());
        	if (cost != 0 && tfname.getText().length() > 0 && tfcontact.getText().length() > 0) {
        		try{
        			FileWriter fw = new FileWriter("Event.txt");
        			 
            		String entree = "none";
            		String sides = "none";
            		String dessert = "none";
            		
            		//If length of a text field is greater than it's default text, we know the user has chosen something.
            		if (tfChoiceEntree.getText().length() > 15) {
                		entree = getChosen(rbSalmon, rbSteak, rbSalad, rbChicken);
            		}
            		if (tfChoiceSide.getText().length() > 14) {
                		sides = getSides(ckRice, ckBroccoli, ckSoup, ckPotato);
            		}
            		if (tfChoiceDessert.getText().length() > 16) {
                		dessert = getChosen(rbCake, rbPudding, rbParfait);
            		}
            		
            		fw.write("Customer name: " + tfname.getText());
            		fw.write("\r\n");
            		fw.write("Customer contact info: " + tfcontact.getText());
            		fw.write("\r\n");
            		fw.write("Number of guests: " + tfguests.getText());
            		fw.write("\r\n");
            		fw.write("Chosen entree: " + entree);
            		fw.write("\r\n");
            		fw.write("Chosen sides: " + sides);
            		fw.write("\r\n");
            		fw.write("Chosen Dessert: " + dessert);
            		
            		
        			fw.close(); 
        			lbresult.setTextFill(Color.LIMEGREEN);
            		lbresult.setText("Validation successful!");   
        		} catch (Exception err) {//Catch exception if any
        		   System.err.println("Error: " + err.getMessage());
        		   lbresult.setText("Error: " + err.getMessage());   
        		}      		
        	} else {
        		lbresult.setTextFill(Color.RED);
        		lbresult.setText("Validation failed!");
        	}
        });

        mainPane.add(btValidate, 0, 7);
        mainPane.add(lbresult, 1, 7);
        
        Scene ckScene = new Scene(mainPane, 1450, 600);  
        
        primaryStage.setTitle("Cindy's Catering");        
        primaryStage.setScene(ckScene); 
   		primaryStage.show();    
   		
    }
    
    public void launchWith(String[] args) {
    	launch(args);
    }
    
    public double parseDoubleIfPossible(String toParse) {
        double parsed = 0;
        try {
      	  parsed = Double.parseDouble(toParse);
  	  }  
  	  catch(NumberFormatException nfe) {
  		  System.out.println("Could not format the string!");
  	  }
        return parsed;
     }
    
    public int parseIntIfPossible(String toParse) {
        int parsed = 0;
        try {
      	  parsed = Integer.parseInt(toParse);
  	  }  
  	  catch(NumberFormatException nfe) {
  		  System.out.println("Could not format the string!");
  	  }
        return parsed;
     }

    public int parseCost(String toParse) {
    	//Will at minimum be $35 if valid... so we check for a minimum length of 3.
    	if (toParse.length() < 3) {
    		return 0;
    	} else {
        	return Integer.parseInt(toParse.substring(1, toParse.length()));    		
    	}
    }
    
    public String getChosen(RadioButton rb1, RadioButton rb2, RadioButton rb3) {
        if(rb1.isSelected()) return rb1.getText();
        else if(rb2.isSelected()) return rb2.getText();
        else if(rb3.isSelected()) return rb3.getText();
        else return "None";
 	}
    
    public String getChosen(RadioButton rb1, RadioButton rb2, RadioButton rb3, RadioButton rb4) {
       if(rb1.isSelected()) return rb1.getText();
       else if(rb2.isSelected()) return rb2.getText();
       else if(rb3.isSelected()) return rb3.getText();
       else if(rb4.isSelected()) return rb4.getText();
       else return "None";
	}
    
    public String getSides(CheckBox ck1, CheckBox ck2, CheckBox ck3, CheckBox ck4) {
        String sides = "";
    	if(ck1.isSelected()) {
        	sides += ck1.getText() + " ";            	
        }
    	if(ck2.isSelected()) {
        	sides += ck2.getText() + " ";            	
        }
    	if(ck3.isSelected()) {
        	sides += ck3.getText() + " ";            	
        }
    	if(ck4.isSelected()) {
        	sides += ck4.getText() + " ";            	
        }
    	
    	if (sides == "") {
    		return "none";
    	} else {
    		return sides;
    	}
    }
    
}
